.. _patchnotes:

Update Notes Archive
********************

This page contains an archive of published update notes dating back to Alpha version `0.1.5`.

..  toctree::
    :caption: Table of Contents
    :name: master-contents
    :maxdepth: 1

    notes-0.1.5
    notes-0.1.6
    notes-0.1.7
    notes-0.2.0
    notes-0.2.1
    notes-0.2.2
    notes-0.2.3
    notes-0.2.4
    notes-0.2.5

