.. _entityAPI:

Game Entities API
*****************

Each Entity type in Foundry Virtual Tabletop extends the base :class:`Entity` class. These implementations and related
functionality are detailed on the following pages.

..  toctree::
    :caption: Entity API
    :maxdepth: 2

    entities/abstract
    entities/user
    entities/scene
    entities/actor
    entities/item
    entities/playlist
    entities/compendium

